package com.schibsted.spain.friends.legacy.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.schibsted.spain.friends.legacy.bean.FriendshipBean;
import com.schibsted.spain.friends.legacy.bean.FriendshipStatusEnum;
import com.schibsted.spain.friends.legacy.bean.User;
import com.schibsted.spain.friends.legacy.exception.FriendshipValidation;
import com.schibsted.spain.friends.legacy.exception.NotFoundException;
import com.schibsted.spain.friends.legacy.storage.Storage;

@RunWith(SpringRunner.class)
public class FriendshipServiceUnitTest {

	@InjectMocks
	private FriendshipService service;

	@MockBean
	private Storage storage;

	private User userFrom;
	private User userTo;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		userFrom = new User("userfrom", "passwordFrom");
		userTo = new User("userto", "passwordTo");
		when(storage.finbyUserNameAndPassword(userFrom.getUserName(), userFrom.getPassword())).thenReturn(userFrom);
		when(storage.finbyUserNameAndPassword(userTo.getUserName(), userTo.getPassword())).thenReturn(userTo);
		when(storage.finbyUserName(userTo.getUserName())).thenReturn(userTo);
		when(storage.finbyUserName(userFrom.getUserName())).thenReturn(userFrom);
	}

	@Test
	public void testRequestNewOK() {
		service.requestFriendship(userFrom.getUserName(), userFrom.getPassword(), userTo.getUserName());
	}

	@Test
	public void testRequestPreviousDeclinedOK() {
		userFrom.getListFriendship().add(new FriendshipBean(userTo.getUserName(), FriendshipStatusEnum.REFUSED));
		service.requestFriendship(userFrom.getUserName(), userFrom.getPassword(), userTo.getUserName());
	}

	@Test(expected = FriendshipValidation.class)
	public void testRequestPreviousAcceptedKO() {
		userFrom.getListFriendship().add(new FriendshipBean(userTo.getUserName(), FriendshipStatusEnum.ACCEPTED));
		service.requestFriendship(userFrom.getUserName(), userFrom.getPassword(), userTo.getUserName());
	}

	@Test(expected = FriendshipValidation.class)
	public void testSameUserKO() {
		service.requestFriendship(userFrom.getUserName(), userFrom.getPassword(), userFrom.getUserName());
	}

	@Test(expected = NotFoundException.class)
	public void testUserFromNotExistKO() {
		when(storage.finbyUserNameAndPassword(userFrom.getUserName(), userFrom.getPassword())).thenReturn(null);
		service.requestFriendship(userFrom.getUserName(), userFrom.getPassword(), userTo.getUserName());
	}

	@Test(expected = NotFoundException.class)
	public void testUserToNotExistKO() {
		when(storage.finbyUserName(userTo.getUserName())).thenReturn(null);
		service.requestFriendship(userFrom.getUserName(), userFrom.getPassword(), userTo.getUserName());
	}

	@Test
	public void testAcceptOK() {
		userTo.getListFriendship().add(new FriendshipBean(userFrom.getUserName(), FriendshipStatusEnum.PENNDING));
		service.acceptFriendship(userFrom.getUserName(), userFrom.getPassword(), userTo.getUserName());
	}

	@Test(expected = FriendshipValidation.class)
	public void testAcceptNotPreviusRequestKO() {
		service.acceptFriendship(userFrom.getUserName(), userFrom.getPassword(), userTo.getUserName());
	}

	@Test(expected = FriendshipValidation.class)
	public void testPreviousAcceptKO() {
		userTo.getListFriendship().add(new FriendshipBean(userFrom.getUserName(), FriendshipStatusEnum.ACCEPTED));
		service.acceptFriendship(userFrom.getUserName(), userFrom.getPassword(), userTo.getUserName());
	}

	@Test
	public void testDeclineOK() {
		userTo.getListFriendship().add(new FriendshipBean(userFrom.getUserName(), FriendshipStatusEnum.PENNDING));
		service.declineFriendship(userFrom.getUserName(), userFrom.getPassword(), userTo.getUserName());
	}

	@Test(expected = FriendshipValidation.class)
	public void testDeclineNotPreviusRequestKO() {
		service.declineFriendship(userFrom.getUserName(), userFrom.getPassword(), userTo.getUserName());
	}

	@Test(expected = FriendshipValidation.class)
	public void testDeclineNotPreviusDeclinedKO() {
		userTo.getListFriendship().add(new FriendshipBean(userFrom.getUserName(), FriendshipStatusEnum.REFUSED));
		service.declineFriendship(userFrom.getUserName(), userFrom.getPassword(), userTo.getUserName());
	}

	@Test
	public void testListOK() {
		userTo.getListFriendship().add(new FriendshipBean(userFrom.getUserName(), FriendshipStatusEnum.ACCEPTED));
		assertFalse(service.listFriends(userTo.getUserName(), userTo.getPassword()).isEmpty());
	}

	@Test
	public void testListEmptyOK() {
		assertTrue(service.listFriends(userTo.getUserName(), userTo.getPassword()).isEmpty());
	}
}

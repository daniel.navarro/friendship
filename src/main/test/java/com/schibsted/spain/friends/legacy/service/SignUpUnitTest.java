package com.schibsted.spain.friends.legacy.service;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.schibsted.spain.friends.legacy.bean.User;
import com.schibsted.spain.friends.legacy.exception.UserPasswordException;
import com.schibsted.spain.friends.legacy.storage.Storage;

@RunWith(SpringRunner.class)
public class SignUpUnitTest {

	@MockBean
	private Storage storage;

	@InjectMocks
	private SignUpService service;

	private User user;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		user = new User("username", "password");
	}

	@Test
	public void testSignUpOK() {
		service.signUp(user);
		verify(storage, times(1)).addUser(user);
	}

	@Test(expected = UserPasswordException.class)
	public void testSignUpKO() {
		when(storage.finbyUserName("username")).thenReturn(user);
		try {
			service.signUp(user);
		} finally {
			verify(storage, never()).addUser(user);
		}
	}

}

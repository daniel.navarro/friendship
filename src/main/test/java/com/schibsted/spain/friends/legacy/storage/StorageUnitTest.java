package com.schibsted.spain.friends.legacy.storage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.schibsted.spain.friends.legacy.bean.FriendshipBean;
import com.schibsted.spain.friends.legacy.bean.FriendshipStatusEnum;
import com.schibsted.spain.friends.legacy.bean.User;

@RunWith(SpringRunner.class)
public class StorageUnitTest {

	private Storage storage;

	private User user1, user2, user3;

	@Before
	public void setUp() throws Exception {
		storage = new Storage();
		user1 = new User("username1", "password");
		storage.getListUser().put("username1", user1);
		user2 = new User("username2", "password");
		storage.getListUser().put("username2", user2);
		user3 = new User("username3", "password");
		storage.getListUser().put("username3", user3);
	}

	@Test
	public void testFinbyUserNameOK() {
		assertEquals(user1, storage.finbyUserName("username1"));
	}

	@Test
	public void testFinbyUserNameKO() {
		assertNull(storage.finbyUserName("usernameX"));
	}

	@Test
	public void testFinbyUserNameAndPasswordOK() {
		assertEquals(user1, storage.finbyUserNameAndPassword(user1.getUserName(), user1.getPassword()));
	}

	@Test
	public void testFinbyUserNameAndPasswordKO() {
		assertNull(storage.finbyUserNameAndPassword(user1.getUserName(), user1.getPassword() + "XX"));
	}

	@Test
	public void testAddUser() {
		int lengthOld = storage.getListUser().size();
		storage.addUser(new User("username4", "password"));
		assertTrue(storage.getListUser().size() == lengthOld + 1);
	}

	@Test
	public void testAddFriendship() {
		FriendshipBean friendshipBean = new FriendshipBean("username2", FriendshipStatusEnum.PENNDING);
		storage.addFriendship(user1, "username2");
		assertEquals(friendshipBean, storage.getListUser().get(user1.getUserName()).getListFriendship().get(0));
	}

	@Test
	public void testAcceptFriendship() {
		storage.addFriendship(user1, "username2");
		storage.acceptFriendship(user1, user2);
		assertEquals(FriendshipStatusEnum.ACCEPTED,
				storage.getListUser().get(user1.getUserName()).getListFriendship().get(0).getStatus());
		assertEquals(FriendshipStatusEnum.ACCEPTED,
				storage.getListUser().get(user2.getUserName()).getListFriendship().get(0).getStatus());
	}

	@Test
	public void testDeclineFriendship() {
		storage.addFriendship(user1, "username2");
		storage.declineFriendship(user1, user2);
		assertEquals(FriendshipStatusEnum.REFUSED,
				storage.getListUser().get(user1.getUserName()).getListFriendship().get(0).getStatus());
	}
}

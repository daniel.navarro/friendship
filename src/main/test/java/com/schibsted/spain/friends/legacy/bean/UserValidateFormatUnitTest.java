package com.schibsted.spain.friends.legacy.bean;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.schibsted.spain.friends.legacy.exception.UserFormatException;

@RunWith(SpringRunner.class)
public class UserValidateFormatUnitTest {

	private User user;

	@Before
	public void setUp() throws Exception {
		user = new User("username", "password");
	}

	@Test
	public void testOK() {
		User.validateFormat(user);
	}

	@Test(expected = UserFormatException.class)
	public void testAplhaNumericUserName() {
		user.setUserName("user?-Name");
		User.validateFormat(user);
	}

	@Test(expected = UserFormatException.class)
	public void testAplhaNumericPassword() {
		user.setPassword("pass?-worde");
		User.validateFormat(user);
	}

	@Test(expected = UserFormatException.class)
	public void testLengthMinUserName() {
		user.setUserName("1234");
		User.validateFormat(user);
	}

	@Test(expected = UserFormatException.class)
	public void testLengthMaxUserName() {
		user.setUserName("12345678901");
		User.validateFormat(user);
	}

	@Test(expected = UserFormatException.class)
	public void testLengthMinPassword() {
		user.setPassword("1234567");
		User.validateFormat(user);
	}

	@Test(expected = UserFormatException.class)
	public void testLengthaxPassword() {
		user.setPassword("1234567890123");
		User.validateFormat(user);
	}

	@Test(expected = UserFormatException.class)
	public void testNullUserName() {
		user.setUserName(null);
		User.validateFormat(user);
	}

	@Test(expected = UserFormatException.class)
	public void testNullPassword() {
		user.setPassword(null);
		User.validateFormat(user);
	}
}

package com.schibsted.spain.friends.legacy.service;

public interface Friendship<F, P, T> {

	void requestFriendship(F userName, P password, T userNameTo) throws RuntimeException;

	void acceptFriendship(F userName, P passwordr, T userNameTo) throws RuntimeException;

	void declineFriendship(F userName, P password, T userNameTo) throws RuntimeException;

	Object listFriends(F userName, P password) throws RuntimeException;

}

package com.schibsted.spain.friends.legacy.bean;

public class FriendshipBean {

	String userNameTo;
	FriendshipStatusEnum status;

	public FriendshipBean(String userNameTo, FriendshipStatusEnum status) {
		super();
		this.userNameTo = userNameTo;
		this.status = status;
	}

	public String getUserNameTo() {
		return userNameTo;
	}

	public void setUserNameTo(String userNameTo) {
		this.userNameTo = userNameTo;
	}

	public FriendshipStatusEnum getStatus() {
		return status;
	}

	public void setStatus(FriendshipStatusEnum status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "FriendShip [userNameTo=" + userNameTo + ", status=" + status + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((userNameTo == null) ? 0 : userNameTo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FriendshipBean other = (FriendshipBean) obj;
		if (status != other.status)
			return false;
		if (userNameTo == null) {
			if (other.userNameTo != null)
				return false;
		} else if (!userNameTo.equals(other.userNameTo))
			return false;
		return true;
	}
}

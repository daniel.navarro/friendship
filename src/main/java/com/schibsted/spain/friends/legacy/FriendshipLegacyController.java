package com.schibsted.spain.friends.legacy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.schibsted.spain.friends.legacy.service.FriendshipService;

@RestController
@RequestMapping("/friendship")
public class FriendshipLegacyController {

	@Autowired
	private FriendshipService service;

	@PostMapping("/request")
	@ResponseStatus(value = HttpStatus.CREATED)
	void requestFriendship(@RequestParam("usernameFrom") String usernameFrom,
			@RequestParam("usernameTo") String usernameTo, @RequestHeader("X-Password") String password) {
		service.requestFriendship(usernameFrom, password, usernameTo);
	}

	@PostMapping("/accept")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	void acceptFriendship(@RequestParam("usernameFrom") String usernameFrom,
			@RequestParam("usernameTo") String usernameTo, @RequestHeader("X-Password") String password) {
		service.acceptFriendship(usernameFrom, password, usernameTo);
	}

	@PostMapping("/decline")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	void declineFriendship(@RequestParam("usernameFrom") String usernameFrom,
			@RequestParam("usernameTo") String usernameTo, @RequestHeader("X-Password") String password) {
		service.declineFriendship(usernameFrom, password, usernameTo);
	}

	@GetMapping("/list")
	@ResponseStatus(value = HttpStatus.OK)
	Object listFriends(@RequestParam("username") String username, @RequestHeader("X-Password") String password) {
		return service.listFriends(username, password);
	}
}

package com.schibsted.spain.friends.legacy.storage;

import java.util.HashMap;

import org.springframework.stereotype.Component;

import com.schibsted.spain.friends.legacy.bean.FriendshipBean;
import com.schibsted.spain.friends.legacy.bean.FriendshipStatusEnum;
import com.schibsted.spain.friends.legacy.bean.User;

@Component
public class Storage implements ManageData<User, String> {

	private HashMap<String, User> listUser;

	public Storage() {
		super();
		listUser = new HashMap<>();
	}

	public HashMap<String, User> getListUser() {
		return listUser;
	}

	@Override
	public User finbyUserName(String userName) {
		return listUser.get(userName);
	}

	@Override
	public User finbyUserNameAndPassword(String userName, String password) {
		return listUser.get(userName) != null && listUser.get(userName).getPassword().equals(password)
				? listUser.get(userName)
				: null;
	}

	@Override
	public void addUser(User user) {
		listUser.put(user.getUserName(), user);

	}

	@Override
	public void addFriendship(User user, String userTo) {
		listUser.get(user.getUserName()).getListFriendship()
				.add(new FriendshipBean(userTo, FriendshipStatusEnum.PENNDING));

	}

	@Override
	public void acceptFriendship(User user, User userTo) {
		listUser.get(user.getUserName()).getListFriendship().stream()
				.filter(f -> f.getUserNameTo().equals(userTo.getUserName())).findFirst().get()
				.setStatus(FriendshipStatusEnum.ACCEPTED);
		listUser.get(userTo.getUserName()).getListFriendship()
				.add(new FriendshipBean(user.getUserName(), FriendshipStatusEnum.ACCEPTED));
	}

	@Override
	public void declineFriendship(User user, User userTo) {
		listUser.get(user.getUserName()).getListFriendship().stream()
				.filter(f -> f.getUserNameTo().equals(userTo.getUserName())).findFirst().get()
				.setStatus(FriendshipStatusEnum.REFUSED);
	}

}

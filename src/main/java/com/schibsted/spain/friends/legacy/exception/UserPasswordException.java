package com.schibsted.spain.friends.legacy.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserPasswordException extends RuntimeException {

	private static final long serialVersionUID = -8680024297092390679L;

	public UserPasswordException(String message) {
		super(message);
	}

}

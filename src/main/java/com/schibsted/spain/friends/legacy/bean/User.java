package com.schibsted.spain.friends.legacy.bean;

import java.util.ArrayList;

import com.schibsted.spain.friends.legacy.exception.UserFormatException;

public class User {

	private String userName;

	private String password;

	ArrayList<FriendshipBean> listFriendship;

	public User(String userName, String password) {
		super();
		this.userName = userName;
		this.password = password;
		this.listFriendship = new ArrayList<>();
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public ArrayList<FriendshipBean> getListFriendship() {
		return listFriendship;
	}

	public void setListFriendship(ArrayList<FriendshipBean> listFriendship) {
		this.listFriendship = listFriendship;
	}

	@Override
	public String toString() {
		return "User [userName=" + userName + ", password=" + password + ", listFriendship=" + listFriendship + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((listFriendship == null) ? 0 : listFriendship.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (listFriendship == null) {
			if (other.listFriendship != null)
				return false;
		} else if (!listFriendship.equals(other.listFriendship))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

	public static void validateFormat(User user) throws UserFormatException {
		if (user.getUserName() == null || !user.getUserName().matches("[A-Za-z0-9]{5,10}")) {
			throw new UserFormatException("fail user name validation [" + user.getUserName() + "]");
		}
		if (user.getPassword() == null || !user.getPassword().matches("[A-Za-z0-9]{8,12}")) {
			throw new UserFormatException("fail password validation [" + user.getPassword() + "]");
		}
	}

}

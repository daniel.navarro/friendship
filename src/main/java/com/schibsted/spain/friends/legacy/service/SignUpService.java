package com.schibsted.spain.friends.legacy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.schibsted.spain.friends.legacy.bean.User;
import com.schibsted.spain.friends.legacy.exception.UserFormatException;
import com.schibsted.spain.friends.legacy.exception.UserPasswordException;
import com.schibsted.spain.friends.legacy.storage.Storage;

@Service
public class SignUpService implements SignUp<User> {

	@Autowired
	private Storage storage;

	@Override
	public void signUp(User user) throws UserFormatException {
		User.validateFormat(user);
		User userDB = storage.finbyUserName(user.getUserName());
		if (userDB == null) {
			storage.addUser(user);
		} else {
			throw new UserPasswordException("this user already singup");
		}
	}

}

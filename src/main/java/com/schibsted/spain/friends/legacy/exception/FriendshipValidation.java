package com.schibsted.spain.friends.legacy.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class FriendshipValidation extends RuntimeException {

	private static final long serialVersionUID = 1574670451379669411L;

	public FriendshipValidation(String exception) {
		super(exception);
	}
}

package com.schibsted.spain.friends.legacy.storage;

public interface ManageData<U, S> {

	U finbyUserName(S userName);

	void addUser(U user);

	U finbyUserNameAndPassword(S userName, S password);

	void addFriendship(U user, S userTo);

	void acceptFriendship(U user, U userTo);

	void declineFriendship(U user, U userTo);
}

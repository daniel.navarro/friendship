package com.schibsted.spain.friends.legacy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.schibsted.spain.friends.legacy.bean.User;
import com.schibsted.spain.friends.legacy.service.SignUpService;

@RestController
@RequestMapping("/signup")
public class SignupLegacyController {

	@Autowired
	private SignUpService service;

	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	void signUp(@RequestParam("username") String username, @RequestHeader("X-Password") String password) {
		service.signUp(new User(username, password));
	}
}

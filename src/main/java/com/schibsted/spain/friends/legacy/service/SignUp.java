package com.schibsted.spain.friends.legacy.service;

import com.schibsted.spain.friends.legacy.exception.UserFormatException;

public interface SignUp<U> {

	void signUp(U user) throws UserFormatException;

}

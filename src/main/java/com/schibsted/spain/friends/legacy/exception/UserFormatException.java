package com.schibsted.spain.friends.legacy.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserFormatException extends RuntimeException {

	private static final long serialVersionUID = -6990403239177907817L;

	public UserFormatException(String exception) {
		super(exception);
	}

}

package com.schibsted.spain.friends.legacy.service;

import java.util.ArrayList;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.schibsted.spain.friends.legacy.bean.FriendshipBean;
import com.schibsted.spain.friends.legacy.bean.FriendshipStatusEnum;
import com.schibsted.spain.friends.legacy.bean.User;
import com.schibsted.spain.friends.legacy.exception.FriendshipValidation;
import com.schibsted.spain.friends.legacy.exception.NotFoundException;
import com.schibsted.spain.friends.legacy.storage.Storage;

@Service
public class FriendshipService implements Friendship<String, String, String> {

	@Autowired
	private Storage storage;

	Predicate<FriendshipBean> requestFriendshipPredicate = (f) -> !f.getStatus().equals(FriendshipStatusEnum.REFUSED);
	Predicate<FriendshipBean> acceptFriendshipPredicate = (f) -> f.getStatus().equals(FriendshipStatusEnum.ACCEPTED);
	Predicate<FriendshipBean> declineFriendshipPredicate = (f) -> !f.getStatus().equals(FriendshipStatusEnum.PENNDING);

	@Override
	public void requestFriendship(String userName, String password, String userNameTo) throws RuntimeException {
		User userFrom = logginUser(userName, password);
		User userTo = searchFriendshipUser(userNameTo);
		validationRequestAction(userFrom, userTo, requestFriendshipPredicate);
		storage.addFriendship(userFrom, userNameTo);

	}

	private void validationRequestAction(User userFrom, User userTo, Predicate<FriendshipBean> condition)
			throws FriendshipValidation {
		checkDifferentUsers(userFrom, userTo);
		userFrom.getListFriendship().stream().filter(f -> f.getUserNameTo().equals(userTo.getUserName())).findFirst()
				.ifPresent(friendshipBean -> {
					if (condition.test(friendshipBean)) {
						throw new FriendshipValidation(
								"There is previous request with status" + friendshipBean.getStatus().toString());
					}
				});
	}

	private void checkDifferentUsers(User userFrom, User userTo) throws FriendshipValidation {
		if (userFrom.getUserName().equals(userTo.getUserName())) {
			throw new FriendshipValidation("A user is the same");
		}
	}

	@Override
	public void acceptFriendship(String userName, String password, String userNameTo) throws RuntimeException {
		User userFrom = logginUser(userName, password);
		User userTo = searchFriendshipUser(userNameTo);
		valiadationAcceptDecline(userFrom, userTo, acceptFriendshipPredicate);
		storage.acceptFriendship(userTo, userFrom);

	}

	private void valiadationAcceptDecline(User userFrom, User userTo, Predicate<FriendshipBean> condition) {
		checkDifferentUsers(userFrom, userTo);
		if (!userTo.getListFriendship().stream().filter(f -> f.getUserNameTo().equals(userFrom.getUserName()))
				.findFirst().isPresent()) {
			throw new FriendshipValidation("This request can't accept,the userFrom is not present");
		} else {
			userTo.getListFriendship().stream().filter(f -> f.getUserNameTo().equals(userFrom.getUserName()))
					.findFirst().ifPresent(friendshipBean -> {
						if (condition.test(friendshipBean)) {
							throw new FriendshipValidation("This accept request can't be executed.");
						}
					});
		}
	}

	@Override
	public void declineFriendship(String userName, String password, String userNameTo) throws RuntimeException {
		User userFrom = logginUser(userName, password);
		User userTo = searchFriendshipUser(userNameTo);
		valiadationAcceptDecline(userFrom, userTo, declineFriendshipPredicate);
		storage.declineFriendship(userTo, userFrom);
	}

	@Override
	public ArrayList<String> listFriends(String userName, String password) throws RuntimeException {
		User user = logginUser(userName, password);
		return (ArrayList<String>) user.getListFriendship().stream()
				.filter(c -> c.getStatus() == FriendshipStatusEnum.ACCEPTED).map(f -> f.getUserNameTo())
				.collect(Collectors.toList());
	}

	private User logginUser(String userName, String password) throws NotFoundException {
		User userDB = storage.finbyUserNameAndPassword(userName, password);
		if (userDB == null) {
			throw new NotFoundException("bad user login");
		}
		return userDB;
	}

	private User searchFriendshipUser(String userNameTo) {
		User userDB = storage.finbyUserName(userNameTo);
		if (userDB == null) {
			throw new NotFoundException("bad user to connect");
		}
		return userDB;
	}

}
